import * as React from "react";
import style from "./index.scss";
import PushDataTag from "highmark/context/PushDataTag";

export default function () {
    return (
        <div className={style.container}>
            <PushDataTag>
                {() => (
                    <>
                        <title>Barebones Highmark App</title>
                        <meta name="description" content="A simple website built with highmark.js" />
                    </>
                )}
            </PushDataTag>
            <div>
                It works!
            </div>
        </div>
    )
}