const nodeExternals = require('webpack-node-externals');
const path = require('path');
const webpack = require('webpack');
const baseConfig = require('./webpack.common.config');
const merge = require('webpack-merge');

const config = {
    name: 'server',
    target: 'node',
    devtool: false,
    entry: {
        server: [
            './.highmark/server/server.ts'
        ]
    },
    node: {
        __dirname: false,
        __filename: false
    },
    output: {
        path: path.join(process.cwd()),
        filename: '[name].js',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.scss|.css$/,
            use: [
                'isomorphic-style-loader',
                { loader: 'css-loader', options: { modules: { model: 'local' } } }, 'sass-loader'
            ]
        }]
    },
    externals: [
        /** Some messed up stuff here to get universal components preloading on server */
        nodeExternals({
            whitelist: [
                '.bin',
                'source-map-support/register',
                /\.(eot|woff|woff2|ttf|otf)$/,
                /\.(svg|png|jpg|jpeg|gif|ico)$/,
                /\.(mp4|mp3|ogg|swf|webp)$/,
                /\.(css|scss|sass|sss|less)$/,
                'highmark',
                v =>
                    v.includes('babel-plugin-universal-import') || v.includes('react-universal-component')

            ]
        })
    ],
    plugins: [
        new webpack.optimize.LimitChunkCountPlugin({
            maxChunks: 1
        })
    ]
};

module.exports = merge.smart(baseConfig, config);
