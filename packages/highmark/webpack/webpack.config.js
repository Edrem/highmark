const path = require('path');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const StatsPlugin = require('stats-webpack-plugin');
const baseConfig = require('./webpack.common.config');
const merge = require('webpack-merge');

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = nodeEnv === 'production';

const config = {
    name: 'client',
    entry: {
        bundle: [
            './.highmark/client.tsx'
        ]
    },
    output: {
        path: path.join(process.cwd(), 'build'),
        filename: '[name].js',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.scss|.css$/,
            use: [
                ExtractCssChunks.loader,
                { loader: 'css-loader', options: { modules: { model: 'local' } } },
                'sass-loader',
                isProd && {
                    loader: 'cssURLReplacer'
                }
            ].filter((e) => e)
        }]
    },
    plugins: [
        new ExtractCssChunks({
            filename: '[name].css',
            chunkFilename: '[id].css'
        }),
        new StatsPlugin('stats.json')
    ],
    optimization: {
        splitChunks: {
            chunks: 'async'
        }
    },
    devtool: 'source-map'
};

if (isProd) {
    config.plugins.push(
        new CompressionPlugin({
            filename: '[path].gz',
            algorithm: 'gzip'
        }),
        new CompressionPlugin({
            filename: '[path].br',
            algorithm: 'brotliCompress',
            compressionOptions: {
                level: 11
            }
        })
    );
    config.optimization.minimizer = [
        new TerserPlugin({
            cache: true,
            parallel: true,
            sourceMap: true
        }),
        new OptimizeCSSAssetsPlugin({})
    ];
}

module.exports = merge.smart(baseConfig, config);
