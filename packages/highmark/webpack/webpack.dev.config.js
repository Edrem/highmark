const base = require('./webpack.config.js');
const merge = require('webpack-merge');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

const config = merge.smart(base, {
    mode: 'development',
    plugins: [
        // new ForkTsCheckerWebpackPlugin(),
        // new HardSourceWebpackPlugin(),
    ]
});

module.exports = config;
