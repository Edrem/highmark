const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const path = require('path');

const nodeEnv = process.env.NODE_ENV || 'development';

const config = {
    mode: nodeEnv,
    module: {
        rules: [{
            test: /\.tsx?$/,
            exclude: /(node_modules)/,
            use: [
                {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/react',
                            '@babel/typescript'
                        ],
                        plugins: [
                            [
                                '@babel/plugin-proposal-decorators',
                                {
                                    legacy: true
                                }
                            ],
                            'babel-plugin-universal-import',
                            '@babel/plugin-syntax-dynamic-import',
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-proposal-optional-chaining',
                            'babel-plugin-transform-react-remove-prop-types'
                        ]
                    }
                }]
        }]
    },
    resolveLoader: {
        modules: ['node_modules', 'node_modules/highmark/loaders']
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        modules: ['highmark/node_modules', 'node_modules'],
        plugins: [
            new TsconfigPathsPlugin({
                configFile: path.resolve(process.cwd(), './tsconfig.json')
            })
        ]
    }
};

module.exports = config;
