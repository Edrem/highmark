import * as React from 'react';
import { btoa } from 'abab';
import HeadTags from '../components/HeadTags';

const HeadDataContext = React.createContext<HeadTagsStore>(null);

type ActionFunction = (...args: any[]) => Promise<any>;

declare global {
    namespace NodeJS {
        interface Global {
            __pushData: {
                [key: string]: object;
            };
        }
    }
}

interface IAction {
    [index: string]: ActionFunction;
}

class HeadTagsStore {
    private tagRenderer: HeadTags;
    public static isServer = !('window' in global);

    public getTagsAsString() {
        if (this.tagRenderer) {
            return this.tagRenderer.renderTags();
        }
        return '';
    }

    public registerTagRenderer(renderer: HeadTags) {
        if (HeadTagsStore.isServer) {
            this.tagRenderer = renderer;
        }
    }
}

export default HeadDataContext;
export { HeadTagsStore };
