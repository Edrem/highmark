import * as React from 'react';
import HeadTagsContext from '../context/HeadTagsContext';
import { renderToString } from 'react-dom/server';

export default class HeadTags extends React.PureComponent<{ children: JSX.Element }> {
    static contextType = HeadTagsContext;
    context: React.ContextType<typeof HeadTagsContext>;

    public renderTags(): string | undefined {
        const { children } = this.props;
        if (!children) {
            return;
        }
        return renderToString(children);
    }

    public async componentDidMount() {
        if (!document.head) {
            return;
        }
        const { children } = this.props;
        if (!children) {
            return;
        }
        let newHead = '';
        newHead = renderToString(children);
        const oldItems = document.head.querySelectorAll('[data-reactroot]');
        oldItems.forEach((item) => {
            if (item) {
                const parent = item.parentNode;
                if (parent) {
                    parent.removeChild(item);
                }
            }
        });
        document.head.innerHTML += newHead;
    }

    public render() {
        this.context.registerTagRenderer(this);
        return null;
    }
}
