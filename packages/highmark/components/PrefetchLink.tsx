// universal((props) => import(/** webpackChunkName: "home" */`./Home`));
import * as React from 'react';
import { LinkProps } from 'react-router-dom';

interface IPrefetchLinkProps extends LinkProps {
    link: Function;
}

export default function PrefetchLink(props: IPrefetchLinkProps) {
    const { children, link: Link, ...linkProps } = props;
    let preloader;
    for (const key of Object.keys(global._routes)) {
        if (key === (linkProps.to.toString())) {
            preloader = global._routes[key].preload;
        }
    }
    return (
        <Link {...linkProps} onMouseOver={preloader}>{children}</Link>
    );
}
