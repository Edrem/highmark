import * as React from 'react';

export default function Image(props: React.HTMLProps<HTMLImageElement>) {
    return (
        <picture>
            {global._assets
                .filter((e) => e.source === props.src)
                .map((e) => {
                    return <source srcSet={`${e.name} ${e.width}w`} type={`image/${e.format}`} />;
                })}
            <img {...props} />
        </picture>
    );
}
