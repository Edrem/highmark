import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { BrowserRouter, Route } from 'react-router-dom';
import App from '../build/app';
import HeadTagsContext, { HeadTagsStore } from 'highmark/context/HeadTagsContext';

// if ('serviceWorker' in navigator) {
//     navigator.serviceWorker.register('/sw.js', { scope: '/' })
//         .then((reg) => {
//             // registration worked
//             console.log('Registration succeeded. Scope is ' + reg.scope);
//         }).catch((error) => {
//             // registration failed
//             console.log('Registration failed with ' + error);
//         });
// }

const pushManager = new HeadTagsStore();

ReactDOM.hydrate(
    <HeadTagsContext.Provider value={pushManager}>
        <BrowserRouter>
            <Route path="/" component={App} />
        </BrowserRouter>
    </HeadTagsContext.Provider>,
    document.getElementById('content')
);
