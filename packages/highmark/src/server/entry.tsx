import { renderToString } from 'react-dom/server';
import { Route, StaticRouter } from 'react-router';
import * as React from 'react';
import flushChunks from 'webpack-flush-chunks';
import { clearChunks, flushChunkNames } from 'react-universal-component/server';
import App from '../../build/app';
import webpackStats from '../../build/stats.json';
import HeadTagsContext, { HeadTagsStore } from 'highmark/context/HeadTagsContext';

export default async function renderPage(context: any, url: string, req: Express.Request): Promise<[string, string[], string[]]> {
    const pushManager = new HeadTagsStore();
    // Clears previous chunks, this could be a big problem with async rendering
    clearChunks();
    const renderedString = renderToString(
        <HeadTagsContext.Provider value={pushManager}>
            <StaticRouter location={url} context={context}>
                <Route path="/" component={App} />
            </StaticRouter>
        </HeadTagsContext.Provider>
    );
    const tags = pushManager.getTagsAsString();
    const { js, css, scripts, stylesheets } = flushChunks(webpackStats, {
        chunkNames: flushChunkNames(),
        after: ['bundle'],
        outputPath: 'build/'
    });

    return [`<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
    ${tags}
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    ${js}
    ${css}
</head>
<body>
    <div id="content">
        ${renderedString}
    </div>
</body>
</html>`,
        scripts,
        stylesheets
    ];
}
