import * as express from 'express';
import * as http from 'http';
import * as path from 'path';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import renderPage from './entry';
import * as compression from 'compression';
import api from '../../build/api';

const app = express();
const devMode = process.argv.slice(2).indexOf('--dev') >= 0;

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());
app.use(cookieParser());

if (!devMode) {
    app.get('*.js', function (req, res, next) {
        if (req.headers['accept-encoding']?.includes('br')) {
            req.url = req.url + '.br';
            res.set('Content-Type', 'application/javascript');
            res.set('Content-Encoding', 'br');
        } else if (req.headers['accept-encoding']?.includes('gzip')) {
            req.url = req.url + '.gz';
            res.set('Content-Type', 'application/javascript');
            res.set('Content-Encoding', 'gzip');
        }
        next();
    });
    app.get('*.css', function (req, res, next) {
        if (req.headers['content-encoding']?.includes('br')) {
            req.url = req.url + '.br';
            res.set('Content-Type', 'text/css');
            res.set('Content-Encoding', 'br');
        } else if (req.headers['content-encoding']?.includes('gzip')) {
            req.url = req.url + '.gz';
            res.set('Content-Type', 'text/css');
            res.set('Content-Encoding', 'gzip');
        }
        next();
    });
}

app.get('/server.js', function (req, res) {
    res.sendStatus(403);
});

app.get('/api/*', (req, res, next) => {
    console.log('Requested API URL', req.url);
    next();
});

app.use('/api', compression());
app.use('/api', api);

app.get('/:wasm.wasm', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.set('Content-Type', 'application/wasm');
    res.sendFile(path.join(__dirname, `/build/${req.params.wasm}.wasm`));
});

app.get('/:js.js', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.js}.js`));
});

app.get('/:js.js.map', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.js}.js.map`));
});

app.get('/:css.css.map', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.css}.css.map`));
});

app.get('/:css.css', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.css}.css`));
});

app.get('/:js.js.gz', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.js}.js.gz`));
});

app.get('/:css.css.gz', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.css}.css.gz`));
});

app.get('/:js.js.br', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.js}.js.br`));
});

app.get('/:css.css.br', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/build/${req.params.css}.css.br`));
});

if (devMode) {
    app.use('/', express.static(path.join(__dirname, 'public'), {
        extensions: ['html', 'jpg', 'png', 'webp', 'ico', 'txt', 'svg'],
    }));
} else {
    app.use('/', express.static(path.join(__dirname, 'build', 'public'), {
        extensions: ['html', 'jpg', 'png', 'webp', 'ico', 'txt', 'svg'],
    }));
}

interface IContext {
    code?: number;
    url?: string;
    extra?: string;
}

/**
 *  Static routing
 */
app.use(compression());
app.get('*', async function (req, res) {
    const context: IContext = {};
    console.log('Requested URL', req.url);
    const t = await renderPage(context, req.url, req);
    if (context.code) {
        if (context.code >= 300 && context.code < 400) {
            res.redirect(context.code, context.extra as string);
        } else {
            res.status(context.code).send(context.extra || '');
        }
    } else {
        const preload = [...t[1].map((file) => `</${file}>; as=script; rel=preload`), ...t[2].map((file) => `</${file}>; as=style; rel=preload`)].join(',');
        //res.set('Link', `${preload}`);
        res.set('Content-Type', 'text/html; charset=utf-8');
        res.send(t[0]);
    }
});

const server = http.createServer(app);
server.listen(8080, () => {
    console.log('HTTP server listening on port 8080');
});

export { server };
export default server;
