declare interface IAsset {
    source: string;
    name: string;
    format: string;
    width: number;
}

declare const _assets: IAsset[];