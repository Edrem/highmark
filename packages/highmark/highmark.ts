#!/usr/bin/env node

import * as commander from 'commander';

commander.version('0.2.3');
commander.option('-d, --debug', 'output extra debugging');

commander.command('production')
    .description('create production build of client/server')
    .action(() => {
        process.env.NODE_ENV = 'production';
        require('./scripts/build');
    });

commander.command('optimize')
    .description('create resized/reformatted versions of image assets')
    .action(() => {
        require('./scripts/optimize');
    });

commander.command('dev-server')
    .description('run the localhost development server')
    .option('--mimic-production', 'Mimic certain production optimizations (code splitting, preloading)')
    .action((args) => {
        console.log(args);
        require('./scripts/watch');
    });

commander.command('analyze')
    .description('open the bundle analyzer')
    .action(() => {
        require('./scripts/analyze');
    });

commander.parse(process.argv);
