import * as fs from 'fs';
import * as path from 'path';
import * as chokidar from 'chokidar';
import * as chalk from 'chalk';
import generateRoutes from './generateRoutes';

import buildConfig from './config';

export default function watchRoutes (onReady: () => void) {
    if (!fs.existsSync(buildConfig.HIGHMARK_FILE_PATH)) {
        fs.mkdirSync(buildConfig.HIGHMARK_FILE_PATH);
    }
    if (!fs.existsSync(path.join(buildConfig.HIGHMARK_FILE_PATH, 'server'))) {
        fs.mkdirSync(path.join(buildConfig.HIGHMARK_FILE_PATH, 'server'));
    }
    fs.copyFileSync(path.join(__dirname, '../../src/client.tsx'), path.join(buildConfig.HIGHMARK_FILE_PATH, 'client.tsx'));
    fs.copyFileSync(path.join(__dirname, '../../src/server/server.ts'), path.join(buildConfig.HIGHMARK_FILE_PATH, 'server', 'server.ts'));
    fs.copyFileSync(path.join(__dirname, '../../src/server/entry.tsx'), path.join(buildConfig.HIGHMARK_FILE_PATH, 'server', 'entry.tsx'));

    const watcher = chokidar.watch([buildConfig.BASE_PAGE_PATH, buildConfig.BASE_API_PATH], {
        ignored: /(^|[\/\\])\../,
        persistent: true,
        cwd: process.cwd()
    });

    const pageRoutes: string[] = [];
    const apiRoutes: string[] = [];

    let hasBuilt = false;
    watcher
        .on('add', addFile)
        // We don't need to monitor file change, because webpack will take over and handle that
        .on('unlink', removeFile)
        .on('ready', () => {
            hasBuilt = true;
            generateRoutes(pageRoutes, apiRoutes);
            onReady();
        });

    const indexMatchRegex = /^.*\.ts|tsx|js|jsx$/g;

    function addFile (path: string) {
        if (path.includes(buildConfig.RELATIVE_PAGE_PATH)) {
            if (indexMatchRegex.test(path)) {
                console.log(`${chalk.bold(chalk.green('[ file ]'))} - Page ${path} has been added`);
                pageRoutes.push(path);
                if (hasBuilt) {
                    generateRoutes(pageRoutes, apiRoutes);
                }
            }
        } else if (path.includes(buildConfig.RELATIVE_API_PATH)) {
            console.log(`${chalk.bold(chalk.green('[ file ]'))} - API ${path} has been added`);
            apiRoutes.push(path);
            if (hasBuilt) {
                generateRoutes(pageRoutes, apiRoutes);
            }
        }
    }

    function removeFile (path: string) {
        if (path.includes(buildConfig.RELATIVE_PAGE_PATH)) {
            if (indexMatchRegex.test(path)) {
                console.log(chalk.red(`File ${path} has been removed`));
                pageRoutes.splice(pageRoutes.findIndex((e) => e === path), 1);
                if (hasBuilt) {
                    generateRoutes(pageRoutes, apiRoutes);
                }
            }
        } else if (path.includes(buildConfig.RELATIVE_API_PATH)) {
            console.log(chalk.red(`File ${path} has been removed`));
            apiRoutes.splice(pageRoutes.findIndex((e) => e === path), 1);
            if (hasBuilt) {
                generateRoutes(pageRoutes, apiRoutes);
            }
        }
    }
}
