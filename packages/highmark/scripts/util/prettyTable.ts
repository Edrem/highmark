import chalk = require('chalk');

function formatBytes (bytes: number) {
    let val = bytes + '  B';
    if (bytes >= 1024) {
        val = (bytes / 1024).toFixed(2) + ' KB';
        if (bytes >= 1024 * 1024) {
            val = (bytes / 1024 / 1024).toFixed(2) + ' MB';
            if (bytes >= 1024 * 1024 * 1024) {
                val = (bytes / 1024 / 1024 / 1024).toFixed(2) + ' GB';
                if (bytes >= 1024 * 1024 * 1024 * 1024) {
                    val = (bytes / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB';
                }
            }
        }
    }
    return val;
}

interface IConfig {
    label: string,
    value: string;
    alignment?: string;
    type?: 'string' | 'bytes';
    width?: number;
    formatter?: (data: string | number) => string;
    colorFormatter?: (data: string | number, output: string) => string;
}

export default function prettyTable (items: { [key: string]: number | string }[], config: IConfig[]) {
    const maxWidth = Math.min(process.stdout.columns || 80, 120);

    const totalWidth = config.reduce((sum, val) => sum + (val.width || 1), 0);
    const columnWidths = config.map((e) => Math.floor(maxWidth * ((e.width || 1) / totalWidth)));

    const computedWidth = columnWidths.reduce((sum, val) => {
        sum += val;
        return sum;
    }, 0);
    if (computedWidth != maxWidth) {
        columnWidths[0] += maxWidth - computedWidth;
    }

    function printHeaderRow () {
        let message = '┌';
        for (let i = 0; i < columnWidths.length; i++) {
            message += '─'.repeat(columnWidths[i]);
            // We don't add this to last item
            if (i < columnWidths.length - 1) {
                message += '┬';
            }
        }
        message += '┐';
        console.log(message);
    }

    function printFooterRow () {
        let message = '└';
        for (let i = 0; i < columnWidths.length; i++) {
            message += '─'.repeat(columnWidths[i]);
            // We don't add this to last item
            if (i < columnWidths.length - 1) {
                message += '┴';
            }
        }
        message += '┘';
        console.log(message);
    }

    function printUnderTitleRow () {
        let message = '├';
        for (let i = 0; i < columnWidths.length; i++) {
            message += '─'.repeat(columnWidths[i]);
            // We don't add this to last item
            if (i < columnWidths.length - 1) {
                message += '┼';
            }
        }
        message += '┤';
        console.log(message);
    }

    function printTitleRow () {
        let message = '│';
        for (let i = 0; i < columnWidths.length; i++) {
            const title = config[i];
            switch (title.alignment || 'left') {
            case 'right':
                message += (config[i].label + ' ').padStart(columnWidths[i]);
                break;
            case 'center':
                const takenWidth = title.label.length;
                message += ' '.repeat(Math.floor((columnWidths[i] - takenWidth) / 2)) +
                        title.label +
                        ' '.repeat(Math.ceil((columnWidths[i] - takenWidth) / 2));
                break;
            case 'left':
            default:
                message += (' ' + config[i].label).padEnd(columnWidths[i]);
            }
            message += '│';
        }
        console.log(chalk.bold(message));
    }

    function printValueRow () {
        for (let a = 0; a < items.length; a++) {
            let message = '│';
            for (let i = 0; i < columnWidths.length; i++) {
                const title = config[i];
                let value: string;
                let originalValue: string | number;
                if (title.type === 'bytes') {
                    originalValue = items[a][config[i].value];
                    value = formatBytes(items[a][config[i].value] as number);
                } else {
                    value = items[a][title.value] as string;
                    originalValue = value;
                }
                if (title.formatter) {
                    originalValue = value;
                    value = title.formatter(value);
                }
                let outputMessage = '';
                switch (title.alignment || 'left') {
                case 'right':
                    outputMessage = (value + ' ').padStart(columnWidths[i]);
                    break;
                case 'center':
                    const takenWidth = value.length;
                    outputMessage = ' '.repeat(Math.floor((columnWidths[i] - takenWidth) / 2)) +
                            value +
                            ' '.repeat(Math.ceil((columnWidths[i] - takenWidth) / 2));
                    break;
                case 'left':
                default:
                    outputMessage = (' ' + value).padEnd(columnWidths[i]);
                }
                if (outputMessage.length > columnWidths[i]) {
                    outputMessage = outputMessage.slice(0, columnWidths[i] - 3) + '...';
                }
                if (title.colorFormatter) {
                    outputMessage = title.colorFormatter(originalValue, outputMessage);
                }
                message += outputMessage;
                message += '│';
            }
            console.log(message);
        }
    }

    printHeaderRow();
    printTitleRow();
    printUnderTitleRow();
    printValueRow();
    printFooterRow();
}
