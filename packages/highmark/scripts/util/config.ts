import * as fs from 'fs';
import * as path from 'path';

let RELATIVE_PAGE_PATH = 'pages';
if (fs.existsSync(path.join('src', 'pages'))) {
    RELATIVE_PAGE_PATH = path.join('src', 'pages');
}

let RELATIVE_API_PATH = 'api';
if (fs.existsSync(path.join('src', 'api'))) {
    RELATIVE_API_PATH = path.join('src', 'api');
}

const BUILD_PATH = path.join(process.cwd(), 'build');
const BASE_PAGE_PATH = path.join(process.cwd(), RELATIVE_PAGE_PATH);
const BASE_API_PATH = path.join(process.cwd(), RELATIVE_API_PATH);
const HIGHMARK_FILE_PATH = path.join(process.cwd(), '.highmark');

export default {
    RELATIVE_PAGE_PATH,
    RELATIVE_API_PATH,
    BUILD_PATH,
    BASE_PAGE_PATH,
    BASE_API_PATH,
    HIGHMARK_FILE_PATH
};
