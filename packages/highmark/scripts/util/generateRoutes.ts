import * as fs from 'fs';
import * as path from 'path';

import buildConfig from './config';

const hasAppCSS = fs.existsSync(path.join(process.cwd(), './src/pages/app.scss'));

export default function generateRoutes(pageRoutes: string[], apiRoutes: string[]) {
    const appOutput = `
import * as React from 'react';
import universal from 'react-universal-component';
import { Route, Switch } from 'react-router-dom';
${hasAppCSS && 'import \'../src/pages/app.scss\';'}

global._routes = {};
global._assets = []; // We don't generate these in dev

${generateRouteImports(pageRoutes)}

export default function App() {
    return (
        <Switch>
            ${generateReactRoutes(pageRoutes)}
        </Switch>
    );
}`;
    if (!fs.existsSync(buildConfig.BUILD_PATH)) {
        fs.mkdirSync(buildConfig.BUILD_PATH);
    }
    fs.writeFileSync(path.join(buildConfig.BUILD_PATH, 'app.tsx'), appOutput);

    const apiOutput = `
import { Router } from 'express';
const router = Router();
${generateApiImports(apiRoutes)}
${generateApiRoutes(apiRoutes)}
export default router;`;
    fs.writeFileSync(path.join(buildConfig.BUILD_PATH, 'api.ts'), apiOutput);
    console.log('Build outputs');
}

const BASE_IMPORT_PATH = '../';
const PAGE_DIR_NAME = buildConfig.RELATIVE_PAGE_PATH.replace(/\\/g, '/');
const API_DIR_NAME = buildConfig.RELATIVE_API_PATH.replace(/\\/g, '/');

function generateRouteImports(routes: string[]) {
    return routes.map((route) => {
        const safeRoute = route.replace(/\\/g, '/');
        const routeWithoutExtension = safeRoute.replace(/(\.[\w\d_-]+)$/i, '');
        const pathName = filepathToURI(safeRoute, PAGE_DIR_NAME);
        const componentName = filepathToComponent(pathName);
        if (true && process.env.NODE_ENV === 'production') {
            return [
                `const ${componentName} = universal(() => import(/** webpackChunkName: "${componentName}" */ '${BASE_IMPORT_PATH}${routeWithoutExtension}'));`,
                `global._routes['${pathName}'] = ${componentName};`,
            ].join('\n');
        } else {
            // Non chunking version in case bundling ever has troubles (mostly issues with analysing build file)
            return `import ${componentName} from "${BASE_IMPORT_PATH}${routeWithoutExtension}";`;
        };
    }).join('\n');
}

function generateApiImports(routes: string[]) {
    return routes.map((route) => {
        const safeRoute = route.replace(/\\/g, '/');
        const routeWithoutExtension = safeRoute.replace(/(\.[\w\d_-]+)$/i, '');
        const pathName = filepathToURI(safeRoute, API_DIR_NAME, true);
        const componentName = filepathToComponent(pathName);
        return `import ${componentName} from "${BASE_IMPORT_PATH}${routeWithoutExtension}";`;
    }).join('\n            ');
}

function generateReactRoutes(routes: string[]) {
    return routes.map((route) => {
        const safeRoute = route.replace(/\\/g, '/');
        const pathName = filepathToURI(safeRoute, PAGE_DIR_NAME);
        const componentName = filepathToComponent(pathName);
        return `<Route path="${pathName}" exact component={${componentName}} />`;
    }).join('\n            ');
}

function generateApiRoutes(routes: string[]) {
    return routes.map((route) => {
        const safeRoute = route.replace(/\\/g, '/');
        const pathName = filepathToURI(safeRoute, API_DIR_NAME, true);
        const componentName = filepathToComponent(pathName);
        return `router.use("${pathName}", ${componentName})`;
    }).join('\n');
}

function filepathToURI(filepath: string, dirname: string, includeFilename: boolean = false) {
    const pathName = /(.+)\/(.+)\..+/g.exec(filepath);
    if (!pathName) {
        throw new Error('Not a valid file');
    }
    const returnedPathName = pathName[1].replace(dirname, '');
    if (!returnedPathName) {
        if (includeFilename) {
            return '/' + pathName[2];
        }
        return '/';
    }
    if (includeFilename) {
        return path.join(returnedPathName, pathName[2]).replace(/\\/g, '/');
    }
    return returnedPathName;
}

function filepathToComponent(filepath: string) {
    return filepath.replace(/\\/g, '/').replace(/\//g, '_');
}
