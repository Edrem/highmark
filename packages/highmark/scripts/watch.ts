import * as webpack from 'webpack';
import * as devConfig from '../webpack/webpack.dev.config';
import * as serverConfig from '../webpack/webpack.server.config';
import * as nodemon from 'nodemon';

import watchRoutes from './util/routeWatcher';
watchRoutes(initServer);

function initServer() {
    const webpackRunner = webpack([devConfig, serverConfig]);
    webpackRunner.run((err, stats) => {
        webpackRunner.watch({}, (err, stats) => {
            // console.log(stats);
        });
        runServer();
    });
}

function runServer() {
    nodemon({
        script: 'server.js',
        args: ['localhost', '8080', '--dev'],
    });
}
