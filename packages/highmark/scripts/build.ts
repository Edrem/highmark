import * as webpack from 'webpack';
import * as clientConfig from '../webpack/webpack.config';
import * as serverConfig from '../webpack/webpack.server.config';

import watchRoutes from './util/routeWatcher';
import chalk = require('chalk');
watchRoutes(initServer);

const {
    performance
} = require('perf_hooks');

function initServer() {
    let then = performance.now();
    const webpackRunner = webpack(clientConfig);
    webpackRunner.run((err, stats) => {
        if (err) {
            console.error(err);
            return;
        }
        if (stats.hasErrors()) {
            console.error(stats.toJson().errors);
        }
        console.log('Client Finished');
        const now = performance.now();
        console.log(chalk.blue(`Time: ${(now - then).toFixed(2)}ms`));
        then = now;
        const webpackRunner = webpack(serverConfig);
        webpackRunner.run((err, stats) => {
            if (err) {
                console.error(err);
                return;
            }
            if (stats.hasErrors()) {
                console.error(stats.toJson().errors);
            }
            const now = performance.now();
            console.log('Server Finished');
            console.log(chalk.blue(`Time: ${(now - then).toFixed(2)}ms`));
            console.log('Build Finished');
            setTimeout(() => process.exit(0), 1000);
        });
    });
}
