process.env.NODE_ENV = "production";
import * as webpack from "webpack";
import * as clientConfig from "../webpack/webpack.config";

import watchRoutes from "./util/routeWatcher";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
watchRoutes(initServer);

function initServer() {
    clientConfig.plugins.push(new BundleAnalyzerPlugin());
    let webpackRunner = webpack(clientConfig);
    webpackRunner.run(() => { });
}
