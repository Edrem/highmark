import * as fs from "fs";
import * as path from "path";
import * as sharp from "sharp";
import * as imagemin from "imagemin";
import imageminPngquant from "imagemin-pngquant";
import prettyTable from "./util/prettyTable";
import chalk = require("chalk");

const baseConfig = require(path.join(process.cwd(), "images.config.json"));

const BUILD_PATH = "build";
const IMAGE_BUILD_PATH = path.join(BUILD_PATH, "public", "images");
const PUBLIC_PATH = "public";
const IMAGE_PATH = path.join(PUBLIC_PATH, "images");

console.log(baseConfig);

if (!fs.existsSync(BUILD_PATH)) {
    fs.mkdirSync(BUILD_PATH);
}
if (!fs.existsSync(path.join(BUILD_PATH, "public"))) {
    fs.mkdirSync(path.join(BUILD_PATH, "public"));
}
if (!fs.existsSync(IMAGE_BUILD_PATH)) {
    fs.mkdirSync(IMAGE_BUILD_PATH);
}

const SIZES: Array<string | number> = [
    "original",
    320,
    800,
    1480,
]

fs.readdir(IMAGE_PATH, async (error, files) => {
    const results: any[] = [];
    for (const file of files) {
        const filePath = path.join(IMAGE_PATH, file);
        const stat = fs.lstatSync(filePath);
        if (stat.isFile()) {
            for (const size of SIZES) {
                const shouldResize = await sharp(filePath)
                    .metadata()
                    .then((metadata) => {
                        if (size === "original") {
                            return false;
                        }
                        if (!metadata.width || metadata.width < size) {
                            return false;
                        }
                        return true;
                    })
                if (!shouldResize && size !== "original") {
                    continue;
                }
                if (true) {
                    /**
                     * Convert to webp, we can do that just with sharp
                     */
                    const convertedFileName = file.replace(/\..+$/g, (shouldResize ? `_${size}` : ``) + ".webp");
                    const converted = await sharp(filePath)
                        .resize((shouldResize && typeof size === "number") ? size : undefined)
                        .webp({
                            lossless: false,
                        })
                        .toFile(path.join(IMAGE_BUILD_PATH, convertedFileName));
                    results.push({
                        old_field: results.find((e) => e.old_field === file) ? "-" : file,
                        new_file: convertedFileName,
                        resolution: `${converted.width}x${converted.height}`,
                        original: stat.size,
                        converted: converted.size,
                        ratio: converted.size / stat.size,
                    });
                }
                if (true) {
                    /**
                     * Resize without converting using sharp, then optimize with imagemin
                     */
                    const convertedFileName = file.replace(/(\.[\w\d_-]+)$/i, shouldResize ? `_${size}$1` : `$1`);
                    const resolution = await sharp(filePath)
                        .resize((shouldResize && typeof size === "number") ? size : undefined)
                        .toFile(path.join(IMAGE_BUILD_PATH, convertedFileName));

                    const imageMinPath = path.join(IMAGE_BUILD_PATH, convertedFileName).replace(/\\/g, "/");
                    await imagemin([imageMinPath], {
                        destination: IMAGE_BUILD_PATH,
                        plugins: [
                            imageminPngquant({
                                quality: [0.65, 0.8]
                            })
                        ]
                    });
                    const newStat = fs.lstatSync(path.join(IMAGE_BUILD_PATH, convertedFileName));
                    results.push({
                        old_field: results.find((e) => e.old_field === file) ? "-" : file,
                        new_file: convertedFileName,
                        resolution: `${resolution.width}x${resolution.height}`,
                        original: stat.size,
                        converted: newStat.size,
                        ratio: newStat.size / stat.size,
                    });
                }
            }
        }
    }
    prettyTable(results, [
        {
            label: "Original Filename",
            value: "old_field",
            width: 2,
            colorFormatter: (data: string, output: string) => {
                if (data === "-") {
                    return chalk.gray(output);
                }
                return chalk.blue(output);
            }
        },
        {
            label: "New Filename",
            value: "new_file",
            width: 2,
        },
        {
            label: "Resolution",
            value: "resolution",
            alignment: "center",
        },
        {
            label: "Original",
            value: "original",
            alignment: "right",
            type: "bytes",
        },
        {
            label: "Converted",
            value: "converted",
            alignment: "right",
            type: "bytes",
        },
        {
            label: "Ratio",
            value: "ratio",
            alignment: "right",
            formatter: (data: number) => {
                if (data >= 1) {
                    return `${(data * 100).toFixed(2)}%`;
                }
                if (data >= 0.9) {
                    return `${(data * 100).toFixed(2)}%`;
                }
                return `${(data * 100).toFixed(2)}%`;
            },
            colorFormatter: (data: number, output: string) => {
                if (data >= 1) {
                    return chalk.red(output);
                }
                if (data >= 0.9) {
                    return chalk.yellow(output);
                }
                return chalk.green(output);
            }
        },
    ]);
});

fs.readdir(PUBLIC_PATH, (error, files) => {
    for (const file of files) {
        const filePath = path.join(PUBLIC_PATH, file);
        const stat = fs.lstatSync(filePath);
        if (stat.isFile()) {
            fs.copyFileSync(filePath, path.join(BUILD_PATH, "public", file));
        }
    }
});